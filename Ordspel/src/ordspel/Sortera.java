package ordspel;

import java.util.Comparator;

/**
 *
 * @author Herikudaru
 */
class Sortera implements Comparator<Spelare> {

    @Override
    public int compare(Spelare a, Spelare b) {
        return b.totalPoang - a.totalPoang;
    }
}
