package ordspel;

import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.text.Text;

/**
 *
 * @author Silentnoodlemaster
 */
class Timer implements Runnable {

    private long startTime;
    private long elapsedTime;
    private long secs;
    private long mins;
    public boolean run;
    public Text text;
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("mm:ss");

    Timer(Text text) {

        this.run = true;
        this.text = text;
    }

    public String stopTime() {
        this.run = false;
        return this.getTime();
    }

    public void resetTimer() {
        this.startTime = System.nanoTime();
    }

    private String getTime() {
        this.elapsedTime = System.nanoTime() - startTime;
        this.mins = TimeUnit.MINUTES.convert(elapsedTime, TimeUnit.NANOSECONDS);
        this.secs = TimeUnit.SECONDS.convert(elapsedTime, TimeUnit.NANOSECONDS);
        this.secs = secs % 60;
        String str = String.format("%02d", mins) + ":" + String.format("%02d", secs);
        return str;
    }

    @Override
    public void run() {
        this.startTime = System.nanoTime();
        while (run) {
            try {
                Thread.sleep(50);
            } catch (InterruptedException ex) {
                Logger.getLogger(Timer.class.getName()).log(Level.SEVERE, null, ex);
            }
            this.text.setText(this.getTime());
        }
    }
}
