package ordspel;

import java.util.ArrayList;

/**
 *
 * @author Silentnoodlemaster
 */
public class Rad {

    ArrayList<Bricka> brickor = new ArrayList<>();

    public void fyll(Pase pase) {
        if (this.brickor.size() < 7) {
            //Bricka bricka = new Bricka("-", 0);
            while ((this.brickor.size() < 7) /* || bricka == null */) {
                if (pase.brickor.size() >= 1) {
                    Bricka bricka = pase.geBricka();
                    this.brickor.add(bricka);
                } else {
                    this.brickor.add(null);
                }
            }
        }
    }

    public boolean harBokstav(String bokstav) {
        boolean check = false;
        ArrayList<String> radLista = new ArrayList<>();
    
        for (int i = 0; i < this.brickor.size(); i++) {
                try {
            radLista.add(this.brickor.get(i).bokstav);
                    }
        catch(NullPointerException e){}
        }
        for (Bricka bricka : this.brickor) {
            if (radLista.contains(bokstav)) {
                radLista.remove(bokstav);
                check = true;
            }
        }

        return check;
    }

    public Bricka RaderaBokstav(String bokstav) {
        for (int i = 0; i < this.brickor.size(); i++) {
            if (this.brickor.get(i).bokstav.equals(bokstav)) {
                return brickor.remove(i);
            }
        }
        return null;
    }

    @Override
    public String toString() {
        String radString = "";
        try {
        for (int i = 0; i < this.brickor.size() - 1; i++) {
            radString += this.brickor.get(i).bokstav;
            radString += Integer.toString(this.brickor.get(i).poang);
            radString += " ";
        }
        radString += this.brickor.get(this.brickor.size() - 1).bokstav;
        radString += Integer.toString(this.brickor.get(this.brickor.size() - 1).poang);
        } catch (NullPointerException e) {}
        return radString;
    }

}
