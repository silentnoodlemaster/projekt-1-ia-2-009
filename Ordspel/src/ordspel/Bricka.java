package ordspel;

/**
 *
 * @author Silentnoodlemaster
 */
public class Bricka {

    protected String bokstav;
    protected int poang, rad, kolumn;

    Bricka(String bokstav, int poang) {
        this.bokstav = bokstav;
        this.poang = poang;
    }

    public void placera(int x, int y) {
        this.rad = x;
        this.kolumn = y;
    }
}
