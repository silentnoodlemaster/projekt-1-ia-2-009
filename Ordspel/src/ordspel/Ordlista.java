/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ordspel;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Silentnoodlemaster
 */
public class Ordlista {

    ArrayList<String> ordlista = new ArrayList<>();
    ArrayList<String> tempOrdlista = new ArrayList<>();

    public void lasFranFil() {
        BufferedReader reader = null;
        FileInputStream fileIn = null;

        try {
            fileIn = new FileInputStream("ordlista.txt");

            reader = new BufferedReader(new InputStreamReader(fileIn));

            String temp = reader.readLine();

            while (temp != null) {
                temp = reader.readLine();
                ordlista.add(temp);
            }
            if (ordlista.get(ordlista.size() - 1) == null) {
                ordlista.remove(ordlista.size() - 1);
            }

        } catch (IOException i) {
            Logger.getLogger(Ordlista.class.getName()).log(Level.SEVERE, null, i);
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
                if (fileIn != null) {
                    fileIn.close();
                }
            } catch (IOException i) {
                Logger.getLogger(Ordlista.class.getName()).log(Level.SEVERE, null, i);
            }
        }

    }

    public boolean existerar(String ord) {
        return this.ordlista.contains(ord);
    }

    public boolean kollaOrd(ArrayList<String> inLista) {
        for (int i = 0; i < inLista.size(); i++) {
            if (existerar(inLista.get(i)) == false) {
                return false;
            }
        }
        return true;
    }

    public ArrayList<String> felAktigaOrd(ArrayList<String> inLista) {
        for (int i = 0; i < inLista.size();) {
            if (existerar(inLista.get(i)) == true) {
                inLista.remove(i);
            } else {
                i++;
            }
        }
        return inLista;
    }

    public void printOrdlista() {
        for (int i = 0; i < ordlista.size(); i++) {
            System.out.println(ordlista.get(i));
        }
    }

    public void setTempOrdLista(ArrayList<String> inList) {
        this.tempOrdlista = (ArrayList<String>) inList;
    }

    public ArrayList getTempOrdLista() {
        return this.tempOrdlista;
    }

    public String getTempOrdListaString() {
        String tempOrdListaString = "";
        for (int i = 0; i < this.tempOrdlista.size(); i++) {
            tempOrdListaString += this.tempOrdlista.get(i);
            tempOrdListaString += " ";
        }
        return tempOrdListaString;
    }

    public void clearTempOrdLista() {
        this.tempOrdlista.clear();
    }
}
