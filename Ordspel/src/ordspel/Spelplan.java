package ordspel;

import java.util.ArrayList;

/**
 *
 * @author Silentnoodlemaster
 */
public class Spelplan {

    protected Ruta[][] rutor;
    protected int n;
    public ArrayList<String> ordlista = new ArrayList<>();

    Spelplan(int n) {
        this.n = n;
        rutor = new Ruta[n][n];
    }

    public Ruta[][] testGetArr() {
        return rutor;
    }

    public void bekrafta(Bricka bricka) {
        rutor[bricka.kolumn][bricka.rad] = new Ruta(bricka);
    }

    public ArrayList<String> allaOrd(String[][] tempPlan) {
        String temp = "";
        int raknare = 0;

        // Horizontal loop
        for (int i = 0; i < rutor.length; i++) {
            for (int j = 0; j < rutor[i].length; j++) {
                if (rutor[i][j] == null) {
                    if (tempPlan[i][j] != null) {
                        raknare++;
                        temp += tempPlan[i][j];
                    } else if (raknare < 2) {
                        raknare = 0;
                        temp = "";
                    } else {
                        ordlista.add(temp);
                        raknare = 0;
                        temp = "";
                    }
                } else {
                    raknare++;
                    temp += rutor[i][j].bricka.bokstav;
                }
            }
        }

        // Vertical loop
        for (int i = 0; i < rutor.length; i++) {
            for (int j = 0; j < rutor[i].length; j++) {
                if (rutor[j][i] == null) {
                    if (tempPlan[j][i] != null) {
                        raknare++;
                        temp += tempPlan[j][i];
                    } else if (raknare < 2) {
                        raknare = 0;
                        temp = "";
                    } else {
                        ordlista.add(temp);
                        raknare = 0;
                        temp = "";
                    }
                } else {
                    raknare++;
                    temp += rutor[j][i].bricka.bokstav;
                }
            }
        }
        return ordlista;
    }
}
