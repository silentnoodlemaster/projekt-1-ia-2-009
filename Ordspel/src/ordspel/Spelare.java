package ordspel;

/**
 *
 * @author Herikudaru
 */
public class Spelare {
    String namn;
    int totalPoang;
    
    public Spelare(int inPoang, String inNamn){
        this.totalPoang = inPoang;
        this.namn = inNamn;
    }
    
    public void adderaTotalPoang(int poang){
        this.totalPoang += poang;
    }
    public void setNamn(String nyNamn){
        this.namn = nyNamn;
    }
    public void lasToppLista(){
        
    }
    public void sparaToppLista(){
        
    }
    @Override
    public String toString(){
        return this.totalPoang + " " + this.namn;
    }
}
