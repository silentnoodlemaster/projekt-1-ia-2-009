package ordspel;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Herikudaru
 */
public class Topplista {

    String toppListaLasare = "";
    int toppListaStorlek = 0;

    Topplista() {

    }

    public void lasFranFil() {
        // format: char <tab> pointValue <tab> count
        BufferedReader reader = null;
        FileInputStream fileIn = null;

        try {
            fileIn = new FileInputStream("topplista.txt");
            reader = new BufferedReader(new InputStreamReader(fileIn));

            String temp = reader.readLine();
            String namn;
            int poang, i = 0;

            while (temp != null) {
                String[] ary = temp.split(":");
                poang = Integer.parseInt(ary[0]);
                namn = ary[1];

                this.toppListaLasare += poang;
                this.toppListaLasare += ":";
                this.toppListaLasare += namn;
                this.toppListaLasare += ":";

                temp = reader.readLine();
                this.toppListaStorlek++;
            }
            this.toppListaLasare = this.toppListaLasare.substring(0, this.toppListaLasare.length() - 1);

        } catch (IOException i) {
            Logger.getLogger(Ordlista.class.getName()).log(Level.SEVERE, null, i);
            i.printStackTrace();
        } finally {
            try {
                if (fileIn != null) {
                    fileIn.close();
                }
            } catch (IOException i) {
                Logger.getLogger(Ordlista.class.getName()).log(Level.SEVERE, null, i);
            }
        }
    }

    public void skrivTillFil(String toppListaString) {
        FileOutputStream fos = null;
        try {
            File fout = new File("topplista.txt");
            fos = new FileOutputStream(fout);
            try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos))) {
                String[] ary = toppListaString.split(":");
                for (int i = 0, j = 0; i < 10; i++, j += 2) {
                    bw.write(ary[j] + ":" + ary[j + 1]);
                    bw.newLine();
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Topplista.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(Topplista.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public String getToppListaLasare() {
        return this.toppListaLasare;
    }

    public Integer getToppListaStorlek() {
        return this.toppListaStorlek;
    }
}
