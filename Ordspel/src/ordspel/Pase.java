package ordspel;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Herikudaru
 */
public class Pase {

    public ArrayList<Bricka> brickor = new ArrayList<>();

    public void laggTillBricka(Bricka bricka) {
        brickor.add(bricka);
    }

    public void lasFranFil() {
        // format: char <tab> pointValue <tab> count
        BufferedReader reader = null;
        FileInputStream fileIn = null;

        try {
            fileIn = new FileInputStream("brickor.txt");
            reader = new BufferedReader(new InputStreamReader(fileIn));

            String temp = reader.readLine();
            String bokstav;
            int poang, antal;

            while (temp != null) {
                String[] ary = temp.split("\\t");
                bokstav = ary[0];
                poang = Integer.parseInt(ary[1]);
                antal = Integer.parseInt(ary[2]);

                for (int i = 0; i < antal; i++) {
                    Bricka bricka = new Bricka(bokstav, poang);
                    laggTillBricka(bricka);
                }
                temp = reader.readLine();
            }

        } catch (IOException i) {
            Logger.getLogger(Ordlista.class.getName()).log(Level.SEVERE, null, i);
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
                if (fileIn != null) {
                    fileIn.close();
                }
            } catch (IOException i) {
                Logger.getLogger(Ordlista.class.getName()).log(Level.SEVERE, null, i);
            }
        }
    }

    public Bricka geBricka() {
        Random rand = new Random();
        int index = 0;
        if (this.brickor.size() > 2) {
            index = rand.nextInt(this.brickor.size() - 1);
        }
        Bricka bricka = this.brickor.get(index);
        if (bricka == null) {
            return null;
        } else {
            this.brickor.remove(index);
            return bricka;
        }
    }
}
