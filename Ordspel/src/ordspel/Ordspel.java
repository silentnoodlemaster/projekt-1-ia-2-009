package ordspel;

import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author Silentnoodlemaster
 */
public class Ordspel extends Application {

    @Override
    public void start(Stage primaryStage) throws IOException {

        // init
        Ordlista ordlista = new Ordlista();
        ordlista.lasFranFil();
        // Scrabble is  15x15 and Alfapet is 17x17
        Spelplan spelplan = new Spelplan(17);

        Pase pase = new Pase();
        pase.lasFranFil();

        Rad rad = new Rad();
        rad.fyll(pase);

        Poang poang = new Poang();
        Spelare spelare = new Spelare(0, "placeholder");

        Topplista toppLista = new Topplista();
        toppLista.lasFranFil();

        GridPane topGrd = new GridPane();
        topGrd.getStyleClass().add("topGrd");

        GridPane grd = new GridPane();
        for (int i = 0; i < spelplan.n; i++) {
            for (int j = 0; j < spelplan.n; j++) {
                TextField txt = new TextField();
                txt.setPrefColumnCount(1); // makes textbox smaller
                txt.setTextFormatter(new TextFormatter<>((TextFormatter.Change change) -> { //limit character count
                    String newText = change.getControlNewText();
                    if (newText.length() > 1) { // max character count is 1
                        return null;
                    } else {
                        return change;
                    }
                }));
                grd.add(txt, j, i);
            }
        }
        grd.setDisable(true);
        Button btn = new Button();
        btn.setText("Placera");

        Button sparKnapp = new Button();
        sparKnapp.setText("Spara poäng");

        Button startKnapp = new Button();
        startKnapp.setText("Start");

        Text nurad = nurad(rad);

        TextField inNamn = new TextField();
        inNamn.getStyleClass().add("namn");

        Text timerText = new Text();
        Timer timer = new Timer(timerText);
        timerText.setText("00:00");

        Label meddelande = new Label();
        meddelande.setText("Ge ditt namn!");

        Text spelareOchPoang = new Text();
        spelareOchPoang.setText(spelare.toString());

        topGrd.add(startKnapp, 0, 0);
        topGrd.add(inNamn, 1, 0);
        topGrd.add(meddelande, 2, 0);
        topGrd.add(btn, 0, 1);
        topGrd.add(timerText, 1, 1);
        topGrd.add(sparKnapp, 0, 2);
        topGrd.add(spelareOchPoang, 1, 2);

        nurad.getStyleClass().add("row");
        BorderPane pane = new BorderPane();
        pane.setCenter(grd);
        pane.setTop(topGrd);
        pane.setBottom(nurad);

        StackPane root = new StackPane();
        root.getChildren().add(pane);

        Scene scene = new Scene(root);
        try {
            scene.getStylesheets().add(
                    getClass().getResource("style.css").toExternalForm()
            );
        } catch (Exception e) {
            System.out.println("css error");
        }

        primaryStage.setTitle("Ordspel");
        primaryStage.setScene(scene);
        primaryStage.show();
        primaryStage.setOnCloseRequest((WindowEvent event) -> {
            timer.run = false;
            primaryStage.close();
        });

        startKnapp.setOnAction((ActionEvent event) -> { // triggers on button click
            spelare.setNamn(inNamn.getText());
            spelareOchPoang.setText(spelare.toString());
            Platform.runLater(() -> {
                Thread thread = new Thread(timer);
                thread.start();
            });
            startKnapp.setDisable(true);
            grd.setDisable(false);
        });

        sparKnapp.setOnAction((ActionEvent event) -> { // triggers on button click
            Spelare[] spelarToppLista = new Spelare[toppLista.toppListaStorlek + 1];
            String[] ary = toppLista.toppListaLasare.split(":");
            for (int i = 0, j = 0; j < ary.length; i++, j += 2) {
                spelarToppLista[i] = new Spelare(Integer.parseInt(ary[j]), ary[j + 1]);
            }

            spelarToppLista[10] = new Spelare(spelare.totalPoang, spelare.namn);
            Arrays.sort(spelarToppLista, new Sortera());
            String toppListaString = "";

            for (int i = 0; i < spelarToppLista.length - 1; i++) {
                toppListaString += spelarToppLista[i].totalPoang;
                toppListaString += ":";
                toppListaString += spelarToppLista[i].namn;
                toppListaString += ":";
            }
            toppListaString = toppListaString.substring(0, toppListaString.length() - 1);
            try {
                toppLista.skrivTillFil(toppListaString);
            } catch (Exception i) {
                Logger.getLogger(Ordspel.class.getName()).log(Level.SEVERE, null, i);
            }
        });

        btn.setOnAction((ActionEvent event) -> { // triggers on button click
            // init tempPlan
            String[][] tempPlan = new String[spelplan.n][spelplan.n];
            boolean giltigRunda = true;

            ObservableList<Node> childrens = grd.getChildren();
            for (Node node : childrens) {

                if (!((TextField) node).getText().isEmpty() && !node.isDisabled()) {
                    String text = ((TextField) node).getText().toUpperCase();
                    if (rad.harBokstav(text)) {
                        int y = GridPane.getColumnIndex(node);
                        int x = GridPane.getRowIndex(node);
                        tempPlan[x][y] = text;
                    } else {
                        System.out.println("you don\'t have " + text);
                        meddelande.setText("Du har inte: " + text);
                        giltigRunda = false;
                    }
                }
            }

            Integer[] ogiltigPlacering = ogiltigPlacering(tempPlan);
            if (ogiltigPlacering[1] == 2) {
                ogiltigPlacering[1] = hittaRiktning(spelplan, tempPlan);
                ogiltigPlacering[2] = hittaIndex(ogiltigPlacering[1], spelplan, tempPlan);
            }
            if (ogiltigPlacering[0] == 0 && giltigRunda) {
                String sammanhangandePlacering = sammanhangandePlacering(ogiltigPlacering[1], ogiltigPlacering[2], spelplan, tempPlan);
                if (sammanhangandePlacering == null) {
                    System.out.println("Fel placerat inte sammanhängande ord");
                    meddelande.setText("Fel placerat inte sammanhängande ord");
                    giltigRunda = false;
                } else {
                    if (!ordlista.existerar(sammanhangandePlacering)) {
                        System.out.println("Ogiltigt ord: " + sammanhangandePlacering);
                        meddelande.setText("Ogiltigt ord: " + sammanhangandePlacering);
                        giltigRunda = false;
                    }
                }
            } else if (giltigRunda) {
                System.out.println("Fel placerat måste placeras på en och samma rad");
                meddelande.setText("Måste placeras på en rad");
                giltigRunda = false;
            }

            ordlista.clearTempOrdLista();
            ordlista.setTempOrdLista(spelplan.allaOrd(tempPlan));

            if (ordlista.kollaOrd(ordlista.getTempOrdLista()) && giltigRunda) {
                // alla ord på planen existerar
            } else if (giltigRunda) {
                ordlista.setTempOrdLista(ordlista.felAktigaOrd(ordlista.getTempOrdLista()));
                for (int i = 0; i < ordlista.getTempOrdLista().size(); i++) {
                    System.out.println("tempOrdlista efter felAktigaOrd har placerats i den: " + i + " " + ordlista.getTempOrdLista().get(i));
                }
                for (int i = 0; i < ordlista.getTempOrdLista().size(); i++) {
                    System.out.print("Felaktiga ord: ");
                    System.out.println(ordlista.getTempOrdLista().get(i));
                }
                meddelande.setText("Felaktiga ord: " + ordlista.getTempOrdListaString());
                giltigRunda = false;
            }

            if (giltigRunda) {
                childrens.stream().filter((node) -> (!((TextField) node).getText().isEmpty() && !node.isDisabled())).forEachOrdered((node) -> {
                    String text = ((TextField) node).getText().toUpperCase();
                    if (rad.harBokstav(text)) {
                        Bricka bricka = rad.RaderaBokstav(text);
                        int x = GridPane.getColumnIndex(node);
                        int y = GridPane.getRowIndex(node);
                        bricka.placera(x, y);
                        spelplan.bekrafta(bricka);
                        node.setDisable(true);
                    }
                });
            }

            if (giltigRunda) {
                poang.raknaPoang(ogiltigPlacering[1], ogiltigPlacering[2], spelplan, tempPlan);
                spelare.adderaTotalPoang(poang.getPoang());
                meddelande.setText(poang.getPoang() + " poäng");
            }

            rad.fyll(pase);
            nurad.setText(rad.toString());
            spelareOchPoang.setText(spelare.toString());
        });

    }

    static Text nurad(Rad rad) {
        Text text = new Text(rad.toString());
        return text;
    }

    public Integer[] ogiltigPlacering(String[][] tempPlan) {
        int horisontal = 0, vertical = 0, hPos = 0, vPos = 0;
        Integer[] giltighetKordinat = {0, 0, 0, tempPlan.length};
        // Horizontal loop
        for (String[] tempPlanX : tempPlan) {
            for (int j = 0; j < tempPlanX.length; j++) {
                if (tempPlanX[j] != null) {
                    hPos = j;
                    horisontal++;
                    j = tempPlan.length - 1;
                }
            }
        }
        // Vertical loop
        for (int i = 0; i < tempPlan.length; i++) {
            for (int j = 0; j < tempPlan[i].length; j++) {
                if (tempPlan[j][i] != null) {
                    vPos = j;
                    vertical++;
                    j = tempPlan.length - 1;
                }
            }
        }
        if (horisontal > 1 && vertical > 1) {
            giltighetKordinat[0] = 1;
        }
        if (horisontal == 1 && vertical == 1) {
            giltighetKordinat[1] = 2;
        } else if (horisontal > 1) {
            giltighetKordinat[1] = 0;
        } else if (vertical > 1) {
            giltighetKordinat[1] = 1;
        }
        if (giltighetKordinat[1] == 0) {
            giltighetKordinat[2] = hPos;
        } else {
            giltighetKordinat[2] = vPos;
        }

        return giltighetKordinat;
    }

    public static Integer hittaRiktning(Spelplan spelplan, String[][] tempPlan) {
        int riktning = 2;
        int x = 0, y = 0;
        boolean top = false, left = false, right = false, bottom = false;
        for (int i = 0; i < spelplan.n; i++) {
            for (int j = 0; j < spelplan.n; j++) {
                if (tempPlan[i][j] != null) {
                    y = i;
                    x = j;
                }
            }
        }
        if (y != 0) {
            if (spelplan.rutor[y - 1][x] != null)
                top = true;
        }
        if (y != spelplan.n - 1) {
            if (spelplan.rutor[y + 1][x] != null)
                bottom = true;
        }
        if (x != 0) {
            if (spelplan.rutor[y][x - 1] != null)
                left = true;
        }
        if (x != spelplan.n - 1){
            if (spelplan.rutor[y][x + 1] != null)
                right = true;
        }
        
        if (top || bottom) {
            riktning = 0;
        } else if (left || right) {
            riktning = 1;
        }

        return riktning;
    }

    public static Integer hittaIndex(int riktning, Spelplan spelplan, String[][] tempPlan) {
        int indexet = 0;
        int x = 0, y = 0;
        boolean top = false, left = false, right = false, bottom = false;
        for (int i = 0; i < spelplan.n; i++) {
            for (int j = 0; j < spelplan.n; j++) {
                if (tempPlan[i][j] != null) {
                    y = i;
                    x = j;
                }
            }
        }
        if (y != 0) {
            if (spelplan.rutor[y - 1][x] != null)
                top = true;
        }
        if (y != spelplan.n - 1) {
            if (spelplan.rutor[y + 1][x] != null)
                bottom = true;
        }
        if (x != 0) {
            if (spelplan.rutor[y][x - 1] != null)
                left = true;
        }
        if (x != spelplan.n - 1){
            if (spelplan.rutor[y][x + 1] != null)
                right = true;
        }
        
        if (top || bottom) {
            indexet = x;
        } else if (left || right) {
            indexet = y;
        }
        return indexet;
    }

    // a står för om ordet är i column eller row, b är indexet
    public static String sammanhangandePlacering(int a, int b, Spelplan spelplan, String[][] tempPlan) {
        boolean ordetArSlut = false;
        boolean ordetHarBorjat = false;
        boolean nyOrd = false;
        boolean felPlacering = false;

        String temp = "";
        if (a == 0) {
            for (int i = 0; i < spelplan.n; i++) {
                if (tempPlan[i][b] == null && !ordetArSlut) {
                    if (spelplan.rutor[i][b] != null) {
                        temp += spelplan.rutor[i][b].bricka.bokstav;
                        ordetHarBorjat = true;
                    } else if (tempPlan[i][b] == null && ordetHarBorjat && spelplan.rutor[i][b] == null) {
                        if (nyOrd) {
                            ordetArSlut = true;
                        } else {
                            ordetHarBorjat = false;
                            ordetArSlut = false;
                            temp = "";
                        }
                    }
                } else if (tempPlan[i][b] != null && !ordetArSlut) {
                    temp += tempPlan[i][b];
                    nyOrd = true;
                    ordetHarBorjat = true;
                } else if (tempPlan[i][b] != null && ordetArSlut) {
                    felPlacering = true;
                }
            }
        } else {
            for (int i = 0; i < spelplan.n; i++) {
                if (tempPlan[b][i] == null && !ordetArSlut) {
                    if (spelplan.rutor[b][i] != null) {
                        temp += spelplan.rutor[b][i].bricka.bokstav;
                        ordetHarBorjat = true;
                    } else if (tempPlan[b][i] == null && ordetHarBorjat && spelplan.rutor[b][i] == null) {
                        if (nyOrd) {
                            ordetArSlut = true;
                        } else {
                            ordetHarBorjat = false;
                            ordetArSlut = false;
                            temp = "";
                        }
                    }
                } else if (tempPlan[b][i] != null && !ordetArSlut) {
                    temp += tempPlan[b][i];
                    ordetHarBorjat = true;
                    nyOrd = true;
                } else if (tempPlan[b][i] != null && ordetArSlut) {
                    felPlacering = true;
                }
            }
        }
        temp = temp.toUpperCase();

        if (felPlacering) {
            temp = null;
        }
        return temp;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
