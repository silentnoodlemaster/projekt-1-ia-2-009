package ordspel;

import java.util.ArrayList;

/**
 *
 * @author Herikudaru
 */
public class Poang {
    int tempPoang;
    int rundaPoang;
    
    Poang(){
        
    }
    public void raknaPoang(int a, int b, Spelplan spelplan, String[][] tempPlan){
        nollStallTempPoang();
        this.rundaPoang = 0;
        boolean ordetArSlut = false;
        boolean ordetHarBorjat = false;
        boolean nyOrd = false;
        ArrayList<Integer> tempKordinat = new ArrayList<>();
        String temp = "";

        if (a == 0) {
            for (int i = 0; i < spelplan.n; i++) {
                if (tempPlan[i][b] == null && !ordetArSlut) {
                    if (spelplan.rutor[i][b] != null) {
                        adderaTempPoang(spelplan.rutor[i][b].bricka.poang);
                        temp += spelplan.rutor[i][b].bricka.bokstav;
                        ordetHarBorjat = true;
                    } else if (tempPlan[i][b] == null && ordetHarBorjat && spelplan.rutor[i][b] == null) {
                        if(nyOrd){
                            ordetArSlut = true;
                        }else{
                            ordetHarBorjat = false;
                            ordetArSlut = false;
                            nollStallTempPoang();
                            tempKordinat.clear();
                            temp = "";
                        }
                    }
                } else if (tempPlan[i][b] != null && !ordetArSlut) {
                    temp += spelplan.rutor[i][b].bricka.bokstav;
                    adderaTempPoang(spelplan.rutor[i][b].bricka.poang);
                    tempKordinat.add(i);
                    nyOrd = true;
                    ordetHarBorjat = true;
                }
            }
        } else {
            for (int i = 0; i < spelplan.n; i++) {
                if (tempPlan[b][i] == null && !ordetArSlut) {
                    if (spelplan.rutor[b][i] != null) {
                        temp += spelplan.rutor[b][i].bricka.bokstav;
                        adderaTempPoang(spelplan.rutor[b][i].bricka.poang);
                        ordetHarBorjat = true;
                    } else if (tempPlan[b][i] == null && ordetHarBorjat && spelplan.rutor[b][i] == null) {
                        if(nyOrd){
                            ordetArSlut = true;
                        }else{
                            ordetHarBorjat = false;
                            ordetArSlut = false;
                            nollStallTempPoang();
                            tempKordinat.clear();
                            temp = "";
                        }
                    }
                } else if (tempPlan[b][i] != null && !ordetArSlut && spelplan.rutor[b][i] != null) {
                    temp += spelplan.rutor[b][i].bricka.bokstav;
                    adderaTempPoang(spelplan.rutor[b][i].bricka.poang);
                    tempKordinat.add(i);
                    ordetHarBorjat = true;
                    nyOrd = true;
                }
            }
        }
        
        this.rundaPoang += this.tempPoang;
        nollStallTempPoang();
        temp = "";
        
        ordetArSlut = false;
        ordetHarBorjat = false;
        nyOrd = false;
        
        for(int k = 0; k < tempKordinat.size(); k++){
            b = tempKordinat.get(k);
            if (a == 1) {
                for (int i = 0; i < spelplan.n; i++) {
                    if (tempPlan[i][b] == null && !ordetArSlut) {
                        if (spelplan.rutor[i][b] != null) {
                            temp += spelplan.rutor[i][b].bricka.bokstav;
                            adderaTempPoang(spelplan.rutor[i][b].bricka.poang);
                            ordetHarBorjat = true;
                        } else if (tempPlan[i][b] == null && ordetHarBorjat && spelplan.rutor[i][b] == null) {
                            if(nyOrd){
                                ordetArSlut = true;
                            }else{
                                ordetHarBorjat = false;
                                ordetArSlut = false;
                                nollStallTempPoang();
                                temp = "";
                            }
                        }
                    } else if (tempPlan[i][b] != null && !ordetArSlut) {
                        nyOrd = true;
                        ordetHarBorjat = true;
                    }
                }
            } else {
                for (int i = 0; i < spelplan.n; i++) {
                    if (tempPlan[b][i] == null && !ordetArSlut) {
                        if (spelplan.rutor[b][i] != null) {
                            temp += spelplan.rutor[b][i].bricka.bokstav;
                            adderaTempPoang(spelplan.rutor[b][i].bricka.poang);
                            ordetHarBorjat = true;
                        } else if (tempPlan[b][i] == null && ordetHarBorjat && spelplan.rutor[b][i] == null) {
                            if(nyOrd){
                                ordetArSlut = true;
                            }else{
                                ordetHarBorjat = false;
                                ordetArSlut = false;
                                nollStallTempPoang();
                                temp = "";
                            }
                        }
                    } else if (tempPlan[b][i] != null && !ordetArSlut) {
                        ordetHarBorjat = true;
                        nyOrd = true;
                    }
                }
            }
        }
        this.rundaPoang += this.tempPoang;
    }
    
    public void adderaTempPoang(int inPoang){
        this.tempPoang = this.tempPoang + inPoang;
    }
    
    public Integer getPoang(){
        return this.rundaPoang;
    }
    
    public void nollStallTempPoang(){
        this.tempPoang = 0;
    }
}
